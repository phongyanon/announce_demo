# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import image.utils


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Upload',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(height_field='height', verbose_name='Image', width_field='width', upload_to=image.utils.UploadPath('uploads'))),
                ('height', models.PositiveIntegerField(editable=False, default='100', null=True, blank=True)),
                ('width', models.PositiveIntegerField(editable=False, default='100', null=True, blank=True)),
                ('add_date', models.DateTimeField(verbose_name='Date added', auto_now_add=True)),
                ('user', models.ForeignKey(verbose_name='Uploaded by', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
