from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from image.utils import UploadPath
from django.db.models.signals import pre_delete
from image.signals import delete_file

class Upload(models.Model):

    path = UploadPath('uploads')

    image = models.ImageField(_('Image'), upload_to=path,
                              height_field='height', width_field='width')
    height = models.PositiveIntegerField(null=True, blank=True, editable=False, default="100")
    width = models.PositiveIntegerField(null=True, blank=True, editable=False, default="100")
    add_date = models.DateTimeField(_('Date added'), auto_now_add=True)
    user = models.ForeignKey(User, verbose_name=_('Uploaded by'))

    def __str__(self):
        return self.image.name

    def image_tag(self):
        return '<img src="{0}" style="width: 100px; height: auto;"/>'.format(self.image.url)

    image_tag.short_description = _('Preview')
    image_tag.allow_tags = True

pre_delete.connect(delete_file, sender=Upload)