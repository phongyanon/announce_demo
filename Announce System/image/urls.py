from django.conf.urls import patterns, url
from image import views

urlpatterns = patterns('',
    url(r'^image_browser/$', views.image_browser, name='image_browser'),
    url(r'^image_uploader/$', views.image_uploader, name='image_uploader'),
)