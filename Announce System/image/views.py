from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from image.models import Upload
import sys


@login_required
def image_browser(request):
    data = Upload.objects.filter(user=request.user).order_by('-add_date')
    callback = request.GET.get('CKEditorFuncNum', None)
    return render_to_response('image/browse.html',
                              {'data': data,
                               'callback': callback})


@csrf_exempt
@login_required
def image_uploader(request):
    callback = request.GET.get('CKEditorFuncNum', None)
    file = request.FILES['upload']
    try:
        upload = Upload(image=file, user=request.user)
        upload.save()
        status = 'Done'
        url = upload.image.url
    except Exception:
        status = str(sys.exc_info())
        url = None
    message = '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction'\
              '({0}, {1}, {2});</script>'.format(callback, url, status)
    return HttpResponse(message, content_type='text/html')