# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('messageapp', '0003_remove_messageannounce_end_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='messageannounce',
            name='end_time',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
