from django.db import models
from django.utils import timezone
from django.utils.timezone import localtime 
import datetime
from datetime import datetime, timedelta

class MessageAnnounce(models.Model):
	message = models.CharField(max_length=200)
	writer = models.CharField(max_length=200, null=True, blank=True, default=None)
	text_owner = models.CharField(max_length=200, null=True, blank=True, default=None)
	create_at = models.DateTimeField(null=True, blank=True)
	start_time = models.DateTimeField(null=True, blank=True)
	end_time = models.DateTimeField(null=True, blank=True)
	def __str__(self):
		return self.message

	def was_published(self): # check show time in 30 seconds
		start_naive = self.start_time.replace(tzinfo=None)+timedelta(hours=7)
		end_naive = self.end_time.replace(tzinfo=None)+timedelta(hours=7)
		return ( datetime.now() >= start_naive )and( end_naive > datetime.now())

	def get_message_time(self): 
		end_naive = self.end_time.replace(tzinfo=None)+timedelta(hours=7)
		message_time = end_naive - datetime.now()#localtime(timezone.now())
		return int(message_time.total_seconds())