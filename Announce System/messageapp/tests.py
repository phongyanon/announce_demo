import unittest
from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from .models import MessageAnnounce

from django.utils import timezone
from django.utils.timezone import localtime
import datetime

class MessageTestCase(TestCase):
	def setUp(self):
		MessageAnnounce.objects.create(message = "Hello message", 
			start_time=localtime(timezone.now()), 
			end_time=localtime(timezone.now()) +datetime.timedelta(seconds=31))

	def test_message_was_publish(self):
		message = MessageAnnounce.objects.get(message = "Hello message")
		self.assertEqual(message.was_published, True)

	def test_new_message_is_coming(self):
		MessageAnnounce.objects.create(message = "Hi there", 
			start_time=localtime(timezone.now()),
			end_time=localtime(timezone.now()) +datetime.timedelta(seconds=31))
		message1 = MessageAnnounce.objects.get(message = "Hi there")
		self.assertEqual(message1.was_published, False)

		MessageAnnounce.objects.create(message = "Hi there2", 
			start_time=localtime(timezone.now()) +datetime.timedelta(seconds=31),
			end_time=localtime(timezone.now()) +datetime.timedelta(seconds=61))
		message2 = MessageAnnounce.objects.get(message = "Hi there2")
		self.assertEqual(message2.was_published, False)

class PostMessageToScreenTest(LiveServerTestCase):
	"""
	1. url messageapp should be worked.
	2. whitespace posting must alert is appear.
	3. post a message and that message should appear.
	4. post two messages the first appear then wait for 30s the secound will appear. 
	"""
	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)

	def tearDown(self):
		self.browser.quit()

	def test_message_page(self):
		self.browser.get(self.live_server_url + '/messageapp')
		body = self.browser.find_element_by_tag_name('body')
		self.assertIn('Message app', body.text)

	def test_post_whitespace_message(self):
		self.browser.get(self.live_server_url + '/messageapp')
		browser.find_element_by_id("Post_button").click()
		show_message = self.browser.find_element_by_class_name("message_posted")
		self.assertNotEqual('', show_message.text)

	def test_post_a_message(self):
		self.browser.get(self.live_server_url + '/messageapp')
		post_area = self.browser.find_element_by_tag_name('textarea')
		post_area.send_keys("Hello world.")
		show_message = self.browser.find_element_by_class_name("message_posted")
		self.assertIn('Hello world', show_message.text)

	def test_post_two_message(self):
		self.browser.get(self.live_server_url + '/messageapp')
		post_area = self.browser.find_element_by_tag_name('textarea')
		post_area.send_keys("Hello world.")
		post_area.send_keys("Hello mars.")
		show_message = self.browser.find_element_by_class_name("message_posted")
		self.assertIn('Hello world', show_message.text)
