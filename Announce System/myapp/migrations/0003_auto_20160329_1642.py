# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
        ('myapp', '0002_articlehello_articlepluginmodel_assonciateditem'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='articlepluginmodel',
            name='cmsplugin_ptr',
        ),
        migrations.RemoveField(
            model_name='assonciateditem',
            name='plugin',
        ),
        migrations.DeleteModel(
            name='ArticlePluginModel',
        ),
        migrations.DeleteModel(
            name='AssonciatedItem',
        ),
    ]
