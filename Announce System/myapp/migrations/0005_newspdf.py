# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import filer.fields.file


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
        ('filer', '0002_auto_20150606_2003'),
        ('myapp', '0004_videoplugineditor'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsPDF',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(serialize=False, to='cms.CMSPlugin', parent_link=True, auto_created=True, primary_key=True)),
                ('title', models.CharField(null=True, default=None, blank=True, max_length=50)),
                ('file', filer.fields.file.FilerFileField(related_name='pdf_disclaimer', blank=True, to='filer.File', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
