$(document).ready(function(){
    // check file type and file size for upload picture viewer.
    $('input[name=password_old]').val("");
    $("input[name=ViewerPictureUploader]").change(function(){
        var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        var f = this.files[0];
        console.log($(this).val());
        console.log('file: '+ f.size.toString());
        // if invalid file type or file size is bigger than 2MB
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1 || f.size > 2097152){ 
            if (f.size > 2097152){ // 20480 test 20kB  2097152
                console.log("file size is bigger than 2MB");
                $('div[name=alert-ImageUploader]').html("<div class='alert alert-danger'>"+'<strong>File size error!</strong>'+
                ' this file size is bigger than 2MB'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>");   //alert file size not support.
            }
            else{
                console.log("wrong file type.");
                $('div[name=alert-ImageUploader]').html("<div class='alert alert-danger'>"+'<strong>File type error!</strong>'+
                ' please use file type: .jpeg, .jpg, .png, .gif, .bmp'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>");   //alert file type not support. 
            }
            $("input[name=ViewerPictureUploader]").val(""); // set input field to empty.
        }
    }); 

    // check file size for csv uploading to create new users.
    $("input[name=userUploader]").change(function(){
        var fileExtension = ['csv'];
        var f = this.files[0];
        console.log($(this).val());
        console.log('file: '+ f.size.toString());
        // if invalid file type or file size is bigger than 2MB
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1 || f.size > 2097152){ 
            if (f.size > 2097152){ // 20480 test 20kB  2097152
                console.log("file size is bigger than 2MB");
                $('div[name=alert-userUploader]').html("<div class='alert alert-danger'>"+'<strong>File size error!</strong>'+
                ' this file size is bigger than 2MB'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>");   //alert file size not support.
            }
            else{
                console.log("wrong file type.");
                $('div[name=alert-userUploader]').html("<div class='alert alert-danger'>"+'<strong>File type error!</strong>'+
                ' please use file type: .csv'+
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+"</div>");   //alert file type not support. 
            }
            $("input[name=userUploader]").val(""); // set input field to empty.
        }
    }); 
});