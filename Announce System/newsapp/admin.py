from django.contrib import admin
from .models import NewsPage, Screen, PageList

admin.site.register(NewsPage)
admin.site.register(Screen)
admin.site.register(PageList)
