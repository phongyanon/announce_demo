#-*- coding: utf-8 -*-
from django import forms

class ImagefileForm(forms.Form):
	ImageUploader = forms.ImageField() 

class PDFfileForm(forms.Form):
	pdfUploader = forms.FileField()

class VideofileForm(forms.Form):
	VideoUploader = forms.FileField()

class VideoURLForm(forms.Form):
	inputVideoURL = forms.CharField(max_length=200, required=False)

class SearchNewsForm(forms.Form):
	search_name = forms.CharField(max_length=100, required=False)
	begin_date = forms.CharField(max_length=100, required=False)
	begin_time = forms.CharField(max_length=100, required=False)
	end_date = forms.CharField(max_length=100, required=False)
	end_time = forms.CharField(max_length=100, required=False)
	search_screen = forms.CharField(max_length=100, required=False)
	tag1 = forms.BooleanField(required=False)
	tag2 = forms.BooleanField(required=False)
	tag3 = forms.BooleanField(required=False)
	tag4 = forms.BooleanField(required=False)