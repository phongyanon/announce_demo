# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
        ('newsapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsPage',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(primary_key=True, auto_created=True, serialize=False, to='cms.CMSPlugin', parent_link=True)),
                ('screen', models.CharField(max_length=50)),
                ('page', models.ForeignKey(to='cms.Page')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.RemoveField(
            model_name='program',
            name='cmsplugin_ptr',
        ),
        migrations.RemoveField(
            model_name='program',
            name='page',
        ),
        migrations.DeleteModel(
            name='Program',
        ),
    ]
