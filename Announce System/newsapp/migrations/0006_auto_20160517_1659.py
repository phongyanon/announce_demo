# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
        ('newsapp', '0005_auto_20160503_1741'),
    ]

    operations = [
        migrations.CreateModel(
            name='Screen',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(to='cms.CMSPlugin', serialize=False, parent_link=True, auto_created=True, primary_key=True)),
                ('name', models.CharField(null=True, default=None, blank=True, max_length=50)),
                ('mute', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.AlterField(
            model_name='newspage',
            name='screen',
            field=models.ForeignKey(to='newsapp.Screen', blank=True, null=True, default=None),
        ),
    ]
