# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsapp', '0011_auto_20160730_1041'),
    ]

    operations = [
        migrations.AddField(
            model_name='pagelist',
            name='url',
            field=models.CharField(default=None, null=True, max_length=100, blank=True),
        ),
    ]
