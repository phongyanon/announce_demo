from cms.models.pluginmodel import CMSPlugin

from django.db import models
from cms.models import CMSPlugin
from django.utils import timezone
from django.utils.timezone import localtime
from datetime import timedelta
import datetime
from cms.models.pagemodel import Page

class Screen(CMSPlugin):
	name = models.CharField(max_length=50,null=True, blank=True, default="untitled")
	mute = models.BooleanField(default=False)

	def __str__(self):
		return self.name

class NewsPage(CMSPlugin):
	name = models.CharField(max_length=50,null=True, blank=True, default="untitled")
	screen = models.ForeignKey(Screen, null=True, blank=True, default=None)
	start_time = models.DateTimeField(null=True, blank=True)
	end_time = models.DateTimeField(null=True, blank=True)
	publish = models.BooleanField(default=False)

	def __str__(self):
		return self.name

	def is_completed(self):
		if (self.name != None) and (self.screen != None) and (self.start_time != None) and (self.end_time != None):
			return True
		else:
			return False

	def my_get_title_page(self, title): # return title of self.page
		all_page = Page.objects.all()
		for each_page in all_page:
			if each_page.get_title_obj('en').title == title:
				return each_page 
		return 

	def was_published(self): # check that is in show time.
		if ( localtime(timezone.now()) >= self.start_time ) and ( localtime(timezone.now()) <= self.end_time ):
			if not self.publish:
				self.set_published()
			return True
		else:
			if self.publish:
				self.clear_published()
			return False

	def set_published(self):
		self.publish = True
		self.save()

	def clear_published(self):
		self.publish = False
		self.save()

	def check_start_time(self, time, set_screen): 
		"""set start_time in condition that no NewsPage have a same 
		start_time in the same screen.
		and no NewsPage have start_time in period of end_time in 
		anathor NewsPage that in the same screen """
		# __lte is mean "less than equal". __gte is mean "greater than equal"
		news = NewsPage.objects.filter(start_time__lte = time, end_time__gt = time, screen=set_screen)
		news = news.exclude(name=self.name)
		if (len( news ) == 0 ):
			if self.end_time != None:
				if self.end_time > time:
					return True
				else:
					return False
			else:
				return True			
		else:
			return False

	def set_start_time(self, time):
		self.start_time = time
		self.save()

	def check_end_time(self, time, set_screen):
		"""check end_time in condition that no NewsPage have end_time on
		period of start_time in anathor NewsPage that in the same screen.
		""" 
		#below line may be valid condition.
		#NewsPage.objects.filter(start_time__lt = end_datetime, end_time__gt = end_datetime, screen=set_screen)
		news = NewsPage.objects.filter(start_time__gt = time, end_time__lte = time, screen=set_screen)
		news = news.exclude(name=self.name)
		if len( news ) == 0 :
			if self.start_time != None:
				if self.start_time < time:
					return True
				else:
					return False
			else:
				return True
		else:
			return False

	def get_refresh_time(self):
		if self.end_time >= localtime(timezone.now()):
			return (self.end_time - localtime(timezone.now())).seconds
		else:
			return 10

	def set_end_time(self, time):
		self.end_time = time
		self.save()

	def clear_start_time(self):
		self.start_time = None
		self.save()

	def clear_end_time(self):
		self.end_time = None
		self.save()

	def clear_all_time(self):
		self.start_time = None
		self.end_time = None
		self.save()

	def check_screen(self, screen):
		""" check screen by no have conflict with other start_time and end_time
		of other NewsPage in the same screen."""
		if self.start_time != None:
			if not self.check_start_time(self.start_time, screen):
				return False

		if self.end_time != None:
			if not self.check_end_time(self.end_time, screen):
				return False

		return True

	def set_screen(self, screen):
		self.screen = screen
		self.save()

	def clear_screen(self):
		self.screen = None
		self.save()

	def set_page(self, set_page):
		self.page = set_page
		self.save()

	def clear_page(self):
		self.page = None
		self.save()

class PageList(CMSPlugin):
	name = models.CharField(max_length=50,null=True, blank=True, default="untitled")
	picture = models.ImageField(upload_to = 'media/page_preview', null=True, blank=True)
	page = models.OneToOneField(Page, null=True, blank=True, default=None)
	interval = models.DurationField(null=True, blank=True, default=timedelta(seconds=30))
	index = models.IntegerField(null=True, blank=True, default=None)
	tag = models.CharField(max_length=50,null=True, blank=True, default=None)
	url = models.CharField(max_length=100,null=True, blank=True, default=None)
	program = models.ForeignKey(NewsPage, null=True, blank=True, default=None)
	prototype = models.IntegerField(null=True, blank=True, default=None)

	def __str__(self):
		return self.name