from django.conf.urls import url 

from . import views
urlpatterns = [
	url(r'^$', views.show_all_screen, name='show_all_screen' ),
	url(r'^list/$', views.get_all_announce, name='get_all_announce' ),
	# news_page
	url(r'^new/$', views.new_announce, name='new_announce'),
	url(r'^(?P<news_id>\d+)/new/$', views.new_announce, name='new_announce'),
	
	url(r'^to_create_announce/$', views.to_create_announce, name='to_create_announce'),
	#url(r'^to_create_announce/$', views.gotocreatepage, name='to_create_announce'),
	url(r'^(?P<news_id>\d+)/to_create_announce/$', views.to_create_announce, name='to_create_announce'),
	url(r'^(?P<news_id>\d+)/(?P<error_message>\w+)/err_create_announce/$', views.to_create_announce, name='err_create_announce'),
	url(r'^(?P<error_message>\w+)/err_create_announce/$', views.to_create_announce, name='err_create_announce'),

	url(r'^to_create_announce/edit_program.json$', views.edit_program, name='edit_program'),
	url(r'^to_create_announce/edit_text.json$', views.edit_text, name='edit_text'),

	url(r'^to_create_announce/page_to_draft.json$', views.page_to_draft, name='page_to_draft'),  # page_to_draft
	url(r'^delete_page_select.json$', views.delete_page_select, name='delete_page_select'), # delete_page_select
	url(r'^add_new_page.json$', views.add_new_page, name='add_new_page'),# add_new_page
	url(r'^(?P<page_id>\d+)/copy_program/$', views.copy_program, name='copy_program'), # duplicate program
	###########  Add plugin  ###########
	url(r'^(?P<page_id>\d+)/add_text_plugin/$', views.add_text_plugin, name='add_text_plugin'),
	url(r'^(?P<page_id>\d+)/add_image_plugin/$', views.add_image_plugin, name='add_image_plugin'),
	url(r'^(?P<page_id>\d+)/add_pdf_plugin/$', views.add_pdf_plugin, name='add_pdf_plugin'),
	url(r'^(?P<page_id>\d+)/add_video_plugin/$', views.add_video_plugin, name='add_video_plugin'),
	#############  Search  ################
	url(r'^to_search/', views.to_search, name='to_search'),
	url(r'^search_news/', views.search_news, name='search_news'),
	url(r'^pagination_news.json$', views.pagination_news, name='pagination_news'),

	url(r'^create_page/$', views.create_announce_page, name='create_announce_page'),
	url(r'^(?P<news_id>\d+)/delete/$', views.delete_announce, name='delete_announce'),
	#page
	url(r'^(?P<page_id>\d+)/example/$', views.show_example, name='show_example'),
	url(r'^(?P<page_id>\d+)/delete_page/$', views.delete_page, name='delete_page'),
	# screen
	url(r'^(?P<screen_text>\w+)/screen/$', views.screen_announce, name='screen_announce'),
	url(r'^(?P<screen_text>\w+)/(?P<time_text>\w+)/screen/$', views.screen_announce, name='screen_announce'),
	url(r'^(?P<screen_id>\d+)/table/$', views.get_time_table, name='get_time_table'),

	url(r'^(?P<screen_id>\d+)/to_create_screen/$', views.to_create_screen, name='to_create_screen'),
	url(r'^to_create_screen/$', views.to_create_screen, name='to_create_screen'),
	url(r'^(?P<screen_id>\d+)/(?P<error_message>\w+)/err_create_screen/$', views.to_create_screen, name='err_create_screen'),
	url(r'^(?P<error_message>\w+)/err_create_screen/$', views.to_create_screen, name='err_create_screen'),
	
	url(r'^(?P<screen_id>\d+)/create_screen/$', views.create_screen, name='create_screen'),
	url(r'^create_screen/$', views.create_screen, name='create_screen'),
	url(r'^screen/$', views.view_screen, name='view_screen'),
	url(r'^(?P<screen_id>\d+)/delete_screen/$', views.delete_screen, name='delete_screen'),

	url(r'^check_online.json$', views.check_online, name='check_online'),
	url(r'^check_symbol_page.json$', views.check_symbol_page, name='check_symbol_page'),
]