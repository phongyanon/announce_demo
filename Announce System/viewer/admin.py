from django.contrib import admin

from .models import UserViewer

admin.site.register(UserViewer)
