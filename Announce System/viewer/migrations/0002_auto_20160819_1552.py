# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viewer', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userviewer',
            name='picture',
            field=models.ImageField(upload_to='profile', null=True, default='profile/circleprofile.png', blank=True),
        ),
    ]
